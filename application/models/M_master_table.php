<?php

Class m_master_table extends CI_Model {


    function getAll(){
        $query = $this->db->query("SELECT MT_ID,MT_BARCODE,MT_SECTIONSTART,MT_SECTIONEND,MT_SECINITIAL,MT_LOCATION, CASE WHEN MT_LOCATION=1 THEN 'PHASE 1' WHEN MT_LOCATION=2 THEN 'PHASE 2' WHEN MT_LOCATION=3 THEN 'PHASE 3' WHEN MT_LOCATION=4 THEN 'PHASE 4' WHEN MT_LOCATION=5 THEN 'PHASE 5' WHEN MT_LOCATION=6 THEN 'PHASE 6' ELSE '-' END AS LOCATION,MT_TABLENO,MT_USERNAME,MT_LASTUPDATE FROM JSTQCDB..M_TABLE");
        return $query->result();
    }


    function getDetail($id){
        $this->db->where('MT_ID',$id);
        $this->db->select('*');
        $this->db->from('JSTQCDB..M_TABLE');
        $q  = $this->db->get()->row();
        return $q;
    }

    

    function checkmaxtableno($mt_secinitial='',$mt_location='',$mt_column=''){
    $query = $this->db->query("select max(mt_tableno) TABLENO from JSTQCDB..M_TABLE where mt_secinitial='$mt_secinitial' and mt_location='$mt_location' and mt_tableno like '$mt_column%'");
    return $query->row();
    }

    function inserttable(){
    $lastupdate     =date('Y-m-d H:i:s');
    $mt_username    =$this->session->userdata('loggedin')['emplcode'];
    // $mt_barcode     =;
    //$mt_tableno     =;   

    $query = $this->db->query("INSERT INTO JSTQCDB..M_TABLE (MT_BARCODE,MT_SECTIONSTART,MT_SECTIONEND  MT_SECINITIAL,MT_LOCATION,MT_TABLENO,MT_STATUS,MT_USERNAME,MT_LASTUPDATE VALUES ('$mt_barcode','$mt_sectionstart','$mt_sectionend','$mt_secinitial',$mt_location,'$mt_tableno',1,$mt_username,'$lastupdate')");
    }


    function insert($data){
        $this->db->insert('JSTQCDB..M_TABLE',$data);
    }

}

?>
