<?php

Class m_update_activity extends CI_Model {

    function getAll(){
        $this->db->select('THD_ID,THD_INSTRUCTIONEXT,THD_LOTEXT,THD_INSTRUCTIONINT,THD_LOTIN,THD_BARCODE,THD_LASTACTIVITY,MA_NAME,MA_CATEGORY,THD_USERID,EM_EMPLNAME,THD_TABLEID,MT_BARCODE,THD_LASTUPDATE');
        $this->db->from('JSTQCDB..T_INSPECTIONHD');
        $this->db->join('JSTQCDB..M_ACTIVITY', 'THD_LASTACTIVITY=MA_ID');
        $this->db->join('JSTQCDB..M_TABLE', 'THD_TABLEID=MT_ID');
        $this->db->join('JINCOMMON..TBMST_EMPLOYEE', 'THD_USERID=EM_EMPLCODE');        
        $q  = $this->db->get();
        return $q->result();


    }

    function getMasterGroup($id){
        $query = $this->db->query("SELECT THD_ID,THD_INSTRUCTIONEXT,THD_LOTEXT,THD_INSTRUCTIONINT,THD_LOTIN,THD_BARCODE,THD_LASTACTIVITY,MA_NAME,MA_CATEGORY,THD_USERID,EM_EMPLNAME,THD_TABLEID,MT_BARCODE,THD_LASTUPDATE 
            FROM JSTQCDB..T_INSPECTIONHD 
            JOIN JSTQCDB..M_ACTIVITY ON THD_LASTACTIVITY=MA_ID
                JOIN JSTQCDB..M_TABLE ON THD_TABLEID=MT_ID
                JOIN JINCOMMON..TBMST_EMPLOYEE ON THD_USERID=EM_EMPLCODE
         where THD_ID=".$id);

        return $query->row();
    }

    function getActivity($activityext,$lastactivity){
        $query="SELECT MA_ID,MA_NAME,MA_CATEGORY,MA_STATUS,MA_USERNAME
            FROM JSTQCDB..M_ACTIVITY WHERE MA_STATUS = 1 ";
        
        if (empty($activityext)) {
            $query .= " AND MA_CATEGORY = 'INTERNAL'";

            if($lastactivity==4) { //option = Stop/Suspend
               $query .= " AND MA_ID !=2";
            }
            if($lastactivity==5) { //option = Finish
               $query .= " AND MA_ID =5";
            }

        
        } else {    
            $query .= " AND MA_CATEGORY = 'EKSTERNAL'";

            if($lastactivity==9) { //option = Stop/Suspend
               $query .= " AND MA_ID !=7";
            }
            if($lastactivity==10) { //option = Finish
               $query .= " AND MA_ID =10";
            }

        }

        //var_dump($query); die();

        $q=$this->db->query($query);

        return $q->result();

    }

    function currentactivity ($id){
    $query =$this->db->query("SELECT THD_LASTACTIVITY FROM JSTQCDB..T_INSPECTIONHD WHERE THD_ID=$id");

      return $query->row();
    }

     function updateinternal($id,$data){
      $lastupdate     =date('Y-m-d H:i:s');
      $lastact        =$data['THD_LASTACTIVITY'];
      $userid         =$data['THD_USERID'];
      $pino           =$data['THD_INSTRUCTIONINT'];
      $lotin          =$data['THD_LOTIN'];
      $movecardcode   =$data['THD_BARCODE'];     
      $tableid        =$data['THD_TABLEID'];
      $previewact     =$data['PREVIEWACT'];

      if($previewact==1){ //Inspection
          if($lastact==2){ //dari Inspection ke Pause
            $this->db->query("UPDATE JSTQCDB..T_INSPECTIONDET SET TDI_ENDACTIVITY=2,TDI_ENDTIME='$lastupdate',TDI_LASTUPDATE='$lastupdate' WHERE TDI_ID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_STARTACTIVITY=1)");

            $this->db->query("INSERT INTO JSTQCDB..T_PAUSETIMEDET (TDP_PARENTID,TDP_STARTPAUSE) SELECT (SELECT TDI_ID FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_STARTACTIVITY=1),'$lastupdate'");

          }
          //var_dump($lastact);die();

          if($lastact==4){ //dari Inspection ke Suspend
            $this->db->query("UPDATE JSTQCDB..T_INSPECTIONDET SET TDI_ENDACTIVITY=4,TDI_ENDTIME='$lastupdate',TDI_LASTUPDATE='$lastupdate' WHERE TDI_ID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_STARTACTIVITY=1)");
          
          }

          if($lastact==5){ //dari Inspection ke Finish
            $this->db->query("UPDATE JSTQCDB..T_INSPECTIONDET SET TDI_ENDACTIVITY=5,TDI_ENDTIME='$lastupdate',TDI_LASTUPDATE='$lastupdate' WHERE TDI_ID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_STARTACTIVITY=1)");
          }
      }



      if($previewact==2){ //Pause
          if($lastact==1){ //dari Pause ke Inspection 
            $this->db->query("INSERT INTO JSTQCDB..T_INSPECTIONDET(TDI_PARENTID,TDI_USERID,TDI_DEVICEID,TDI_TABLEID,TDI_STARTACTIVITY,TDI_STARTTIME,TDI_LASTUPDATE) SELECT (SELECT THD_ID FROM JSTQCDB..T_INSPECTIONHD WHERE THD_INSTRUCTIONINT=$pino AND THD_LOTIN=$lotin AND THD_USERID=$userid),$userid,6,$tableid,1,'$lastupdate','$lastupdate'");


            $this->db->query("UPDATE JSTQCDB..T_PAUSETIMEDET SET TDP_ENDPAUSE='$lastupdate' WHERE TDP_PARENTID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_ENDACTIVITY=2)");

          }

          if($lastact==4){ //dari Pause ke Suspend
            $this->db->query("UPDATE JSTQCDB..T_INSPECTIONDET SET TDI_ENDACTIVITY=4,TDI_ENDTIME='$lastupdate',TDI_LASTUPDATE='$lastupdate' WHERE TDI_ID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_STARTACTIVITY=1)");

            $this->db->query("UPDATE JSTQCDB..T_PAUSETIMEDET SET TDP_ENDPAUSE='$lastupdate' WHERE TDP_PARENTID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_ENDACTIVITY=2)");

            }

          if($lastact==5){ //dari Pause ke Finish
            $this->db->query("UPDATE JSTQCDB..T_INSPECTIONDET SET TDI_ENDACTIVITY=5,TDI_ENDTIME='$lastupdate',TDI_LASTUPDATE='$lastupdate' WHERE TDI_ID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_STARTACTIVITY=1)");

            $this->db->query("UPDATE JSTQCDB..T_PAUSETIMEDET SET TDP_ENDPAUSE='$lastupdate' WHERE TDP_PARENTID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_ENDACTIVITY=2)");

            }

//var_dump($lotin);die();
      }

      if($previewact==4) { //Suspend
          if(($lastact==1) or ($lastact==5)){ //dari Suspend ke Inspection atau ke Finish
          $this->db->query("INSERT INTO JSTQCDB..T_INSPECTIONDET(TDI_PARENTID,TDI_USERID,TDI_DEVICEID,TDI_TABLEID,TDI_STARTACTIVITY,TDI_STARTTIME,TDI_LASTUPDATE) SELECT (SELECT THD_ID FROM JSTQCDB..T_INSPECTIONHD WHERE THD_INSTRUCTIONINT='$pino' AND THD_LOTIN='$pino' AND THD_BARCODE='$movecardcode' AND THD_USERID=$userid),$userid,6,$tableid,6,'$lastupdate','$lastupdate'");
          }  
      }

    }


    function updateexternal($id,$data){
      $lastupdate     =date('Y-m-d H:i:s');
      $lastact        =$data['THD_LASTACTIVITY'];
      $userid         =$data['THD_USERID'];
      $pino           =$data['THD_LOTEXT'];
      $movecardcode   =$data['THD_BARCODE'];     
      $tableid        =$data['THD_TABLEID'];
      $previewact     =$data['PREVIEWACT'];

      if($previewact==6){ //Inspection
          
          if($lastact==7){ //dari Inspection ke Pause
            $this->db->query("UPDATE JSTQCDB..T_INSPECTIONDET SET TDI_ENDACTIVITY=7,TDI_ENDTIME='$lastupdate',TDI_LASTUPDATE='$lastupdate' WHERE TDI_ID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_STARTACTIVITY=6)");

            $this->db->query("INSERT INTO JSTQCDB..T_PAUSETIMEDET (TDP_PARENTID,TDP_STARTPAUSE) SELECT (SELECT TDI_ID FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_STARTACTIVITY=6),'$lastupdate'");
          }


          if($lastact==9){ //dari Inspection ke Suspend
            $this->db->query("UPDATE JSTQCDB..T_INSPECTIONDET SET TDI_ENDACTIVITY=9,TDI_ENDTIME='$lastupdate',TDI_LASTUPDATE='$lastupdate' WHERE TDI_ID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_STARTACTIVITY=6)");
          }


          if($lastact==10){ //dari Inspection ke Finish
            $this->db->query("UPDATE JSTQCDB..T_INSPECTIONDET SET TDI_ENDACTIVITY=10,TDI_ENDTIME='$lastupdate',TDI_LASTUPDATE='$lastupdate' WHERE TDI_ID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_STARTACTIVITY=6)");
          }
      }

      if($previewact==7){ //Pause
          if($lastact==6){ //dari Pause ke Inspection
            $this->db->query("INSERT INTO JSTQCDB..T_INSPECTIONDET(TDI_PARENTID,TDI_USERID,TDI_DEVICEID,TDI_TABLEID,TDI_STARTACTIVITY,TDI_STARTTIME,TDI_LASTUPDATE) SELECT (SELECT THD_ID FROM JSTQCDB..T_INSPECTIONHD WHERE THD_INSTRUCTIONEXT='$pino' AND THD_LOTEXT='$pino' AND THD_BARCODE='$movecardcode' AND THD_USERID=$userid),$userid,6,$tableid,6,'$lastupdate','$lastupdate'");

            $this->db->query("UPDATE JSTQCDB..T_PAUSETIMEDET SET TDP_ENDPAUSE='$lastupdate' WHERE TDP_PARENTID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_ENDACTIVITY=7)");

            }

          if($lastact==9){ //dari Pause ke Suspend
            $this->db->query("UPDATE JSTQCDB..T_INSPECTIONDET SET TDI_ENDACTIVITY=9,TDI_ENDTIME='$lastupdate',TDI_LASTUPDATE='$lastupdate' WHERE TDI_ID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_STARTACTIVITY=6)");

            $this->db->query("UPDATE JSTQCDB..T_PAUSETIMEDET SET TDP_ENDPAUSE='$lastupdate' WHERE TDP_PARENTID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_ENDACTIVITY=7)");

            }

          if($lastact==10){ //dari Pause ke Finish
            $this->db->query("UPDATE JSTQCDB..T_INSPECTIONDET SET TDI_ENDACTIVITY=10,TDI_ENDTIME='$lastupdate',TDI_LASTUPDATE='$lastupdate' WHERE TDI_ID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_STARTACTIVITY=6)");

            $this->db->query("UPDATE JSTQCDB..T_PAUSETIMEDET SET TDP_ENDPAUSE='$lastupdate' WHERE TDP_PARENTID=(SELECT MAX(TDI_ID) FROM JSTQCDB..T_INSPECTIONDET WHERE TDI_USERID=$userid AND TDI_ENDACTIVITY=7)");

            }


        }
      
      if($previewact==9) { //Suspend
          if(($lastact==6) or ($lastact==10)){ //dari Suspend ke Inspection atau ke Finish
          $this->db->query("INSERT INTO JSTQCDB..T_INSPECTIONDET(TDI_PARENTID,TDI_USERID,TDI_DEVICEID,TDI_TABLEID,TDI_STARTACTIVITY,TDI_STARTTIME,TDI_LASTUPDATE) SELECT (SELECT THD_ID FROM JSTQCDB..T_INSPECTIONHD WHERE THD_INSTRUCTIONEXT='$pino' AND THD_LOTEXT='$pino' AND THD_BARCODE='$movecardcode' AND THD_USERID=$userid),$userid,6,$tableid,6,'$lastupdate','$lastupdate'");
          } 
      }
    }


    function update($id,$data){
      $lastupdate=date('Y-m-d H:i:s'); //var_dump($data['THD_LASTACTIVITY']); die;

      $query = $this->db->query("UPDATE JSTQCDB..T_INSPECTIONHD SET THD_LASTACTIVITY=".$data['THD_LASTACTIVITY'].",THD_LASTUPDATE='$lastupdate' WHERE THD_ID=$id");

      if (empty($data['THD_LOTEXT'])) {
            $this->updateinternal($id,$data);
      }else{
            $this->updateexternal($id,$data);
      }
      //var_dump($data['THD_LASTACTIVITY']); die;


        //return $query->row();




        // $this->db->where('THD_ID',$id);
        // $this->db->update('JSTQCDB..T_INSPECTIONHD',$data);

        //         var_dump($query); die();

    }

}


?>
