    <?php

Class model_ extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->DB1 = $this->load->database('firebird',TRUE);
    }

    function userMenu($id) {
        $query = $this->DB1->query("select M_MENUGROUP.GROUPNAME UMG_GroupName, M_MENUGROUP.GROUPCODE UMG_GroupCode,
        M_MENUNAME.MN_MENUNAME as MenuName, M_MENUNAME.MN_MENUNAME UMN_MenuName, M_APPLICATION.APPCODE APPID
        from M_MENUNAME
        inner join TBUA_USERMENUSETUP on M_MENUNAME.MN_MENUCODE = TBUA_USERMENUSETUP.UMS_MENUID inner join
        M_MENUGROUP on M_MENUNAME.MN_GROUPCODE = M_MENUGROUP.GROUPCODE
        inner join M_APPLICATION on M_MENUGROUP.GROUPAPPCODE = M_APPLICATION.APPCODE
        inner join TBUA_USERACCESS on TBUA_USERACCESS.UA_USERID = TBUA_USERMENUSETUP.UMS_USERID 
        where UA_USERID = ".$id." and
        upper(M_APPLICATION.APPNAME) = 'QWORD' and TBUA_USERMENUSETUP.UMS_VIEW = 1
        order by M_MENUGROUP.GROUPNAME, M_MENUNAME.MN_MENUNAME");
        
        return $query->result();
    }

    function login($username,$password) {
        $this->DB1->select('UA_USERID, UA_USERNAME, UA_ROLESCOMMON,UA_EMPLCODE');
        $this->DB1->from('TBUA_USERACCESS');
        $this->DB1->where('UA_USERNAME', $username);
        //$this->DB1->where('UUSERPWD', $password);
        $this->DB1->limit(1);
        $query = $this->DB1->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

}

?>
