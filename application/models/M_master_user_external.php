<?php

Class m_master_user_external extends CI_Model {
    function insert($data){
        $this->db->insert('JSTQCDB..M_ACCESSEXTERNAL',$data);
    }


    function getAll(){
        $query = $this->db->query("SELECT MAE_USERID,MAE_USERNAME,MAE_COMPANY,SPL_SUPNAME,MAE_LASTUSER,MAE_LASTUPDATE 
            FROM JSTQCDB..M_ACCESSEXTERNAL
            INNER JOIN JSTPPSDB..M_SUPPLIER ON SPL_SUPCODE=MAE_COMPANY");
        return $query->result();
    }


    function getDetail($id){
        $this->db->where('MAE_USERID',$id);
        $this->db->select('*');
        $this->db->from('JSTQCDB..M_ACCESSEXTERNAL');
        $q  = $this->db->get()->row();
        return $q;
    }

    function companylist(){
        $query = $this->db->query("SELECT SPL_SUPCODE,SPL_SUPNAME FROM JSTPPSDB..M_SUPPLIER");
        return $query->result();
    }


    function getMasterGroup($id){
        $query = $this->db->query("SELECT MAE_USERID,MAE_USERNAME,MAE_COMPANY,SPL_SUPNAME,MAE_LASTUSER,MAE_LASTUPDATE 
            FROM JSTQCDB..M_ACCESSEXTERNAL
            INNER JOIN JSTPPSDB..M_SUPPLIER ON SPL_SUPCODE=MAE_COMPANY WHERE MAE_USERID=$id");
        return $query->row();
    }

    function update($id,$data){
        $this->db->where('MAE_USERID',$id);
        $this->db->update('JSTQCDB..M_ACCESSEXTERNAL',$data);
    }


}

?>
