<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Input_scrap extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_input_scrap', '',TRUE);
        $this->load->helper(array('form'));
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        if ($this->session->userdata('loggedin')) {
            $data = array (
                'pagetitle'   =>  "Input - Scrap",
                'pos_parent'  =>  "inspection",
                'pos_child'   =>  "inspection/Input_scrap",
                'title'       =>  "Inspection",
                'subtitle'    =>  "List",
                'data'        =>  $this->m_input_scrap->getAll(),
                'plugins_js'  => array('assets/js/datatables.buttons.min.js',
                  'assets/js/buttons.flash.min.js',
                  'assets/js/jszip.min.js',
                  'assets/js/pdfmake.min.js',
                  'assets/js/vfs_fonts.js',
                  'assets/js/buttons.html5.min.js',
                  'assets/js/buttons.print.min.js'),
                'plugins_css'   => array('assets/css/buttons.datatables.min.css'),
                'breadcrumb'  =>   array('<a>Inspection</a>','List'),
                'content'     =>  'inspection/input_scrap/List'
            );
            $this->load->view('template/page', $data);
        } else {
            redirect(base_url().'login', 'refresh');
        }
    }

    public function edit($id = '') {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('thd_judgement', 'Judgement', 'trim|required');
            $this->form_validation->set_rules('thd_inspectionqty', 'Inspection Qty', 'trim|required');
            $this->form_validation->set_rules('thd_qtyng', 'Qty NG', 'trim|required');
            //$judgement = $this->m_input_scrap->getMasterGroup($id);
            $activityext = $this->m_input_scrap->getMasterGroup($id);
            if ($this->form_validation->run() == FALSE) {
                $data = array (
                    'pagetitle'   =>  "Input - Scrap",
                    'pos_parent'  =>  "inspection",
                    'pos_child'   =>  "inspection/Input_scrap",
                    'title'       =>  "Inspection",
                    'subtitle'    =>  "Edit",
                    'action'      => "<a class='button button-red' href='".base_url()."inspection/input_scrap'><i class='fa fa-times'></i> Cancel &amp; Discard</a>",
                    'breadcrumb'  =>   array('<a>Inspection</a>','Edit'),
                    // 'plugins_css'  => array('plugins/jquery-te/jquery-te-1.4.0.css'),
                    // 'plugins_js'   => array('plugins/jquery-te/jquery-te-1.4.0.min.js'),
                    'ma'          => $this->m_input_scrap->getMasterGroup($id,true,$activityext->THD_LOTEXT),
                    'defect1'     => $this->m_input_scrap->defecttype1(),
                    // 'us'          => $this->m_update_activity->getActivity($id,$activityext->THD_LOTEXT),                    
                    // 'brt'          => $this->m_berita->getBeritaId($id),
                    'content'     =>  'inspection/input_scrap/Edit',


                );

                $type1=$data['ma']->THD_ABNORMAL1;
                $type2=$data['ma']->THD_ABNORMAL2;
                $type3=$data['ma']->THD_ABNORMAL3;                                
                if((isset($type1)) and(isset($type2)) and (isset($type3))){
                    $data['defect2']=$this->m_input_scrap->defecttype2($type1);
                    $data['defect3']=$this->m_input_scrap->defecttype3($type2);                    
                }else {
                    $data['defect2']='';
                    $data['defect3']='';
                }

                //var_dump($data); die();
                $this->load->view('template/page', $data);
            } else {
                $data = array(
                    'THD_JUDGEMENT'       => $this->input->post('thd_judgement'),
                    'THD_INSPECTIONQTY'   => $this->input->post('thd_inspectionqty'),
                    'THD_QTYNG'           => $this->input->post('thd_qtyng'),
                    'THD_ABNORMAL1'       => $this->input->post('thd_abnormal1'),
                    'THD_ABNORMAL2'       => $this->input->post('thd_abnormal2'),
                    'THD_ABNORMAL3'       => $this->input->post('thd_abnormal3'),
                    //$mt_username    =$this->session->userdata('loggedin')['emplcode'];

                );

                $this->m_input_scrap->update($id,$data);
                $this->session->set_flashdata("pesan", "<div class='alert alert-success'>
                     <p><b>Success!</b> Inspection Input Scrap Edited.<i class='fa fa-times'></i></p>
                 </div>");
                redirect(base_url() . 'inspection/input_scrap', 'location');    
            }
        } else {
            //If no session, redirect to login page
            redirect(base_url().'login', 'refresh');
        }
    }

    public function getabnormal2(){
        //var_dump($_POST['ABL1_Code']); die();
        $defect2=$this->m_input_scrap->defecttype2($_POST['ABL1_Code']);
        $output='<option value="0">-SELECT DEFECT TYPE 2-</option>';
        foreach ($defect2 as $d2) {
            $output.='<option value="'.$d2->ABL2_Code.'"> '.$d2->ABL2_StandardCode.' - '.$d2->ABL2_Desc.' </option>';
               }
        echo $output;              
    }

    public function getabnormal3(){
        $defect2=$this->m_input_scrap->defecttype3($_POST['ABL2_Code']);
        $output='<option value="0">-SELECT DEFECT TYPE 3-</option>';
        foreach ($defect2 as $d2) {
            $output.='<option value="'.$d2->ABL3_Code.'">'.$d2->ABL3_StandardCode.' - '.$d2->ABL3_Desc.'</option>';

               }
        echo $output;              
    }    


    // public function delete($id) {
    //     if ($this->session->userdata('loggedin')) {

    //         $this->m_input_scrap->delete($id);
    //         $this->session->set_flashdata("pesan", "<div class='alert alert-success'>
    //             <p><b>Success!</b> Inspection Deleted.<i class='fa fa-times'></i></p>
    //         </div>");
    //         redirect(base_url().'inspection/input_scrap', 'refresh');
    //     } else {
    //         //If no session, redirect to login page
    //         redirect(base_url().'login', 'refresh');
    //     }
    // }
    public function export_excel() {
        if ($this->session->userdata('loggedin')) {
            $data = array (
                'data'        =>  $this->m_input_scrap->exportexcel()
            );
            $this->load->view('inspection/input_scrap/exportexcel', $data);
        } else {
            redirect(base_url().'login', 'refresh');
        }
    }
}
