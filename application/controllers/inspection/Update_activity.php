<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Update_activity extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_update_activity', '',TRUE);
        $this->load->helper(array('form'));
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        if ($this->session->userdata('loggedin')) {
            $data = array (
                'pagetitle'   =>  "Update - Activity",
                'pos_parent'  =>  "inspection",
                'pos_child'   =>  "inspection/Update_activity",
                'title'       =>  "Inspection",
                'subtitle'    =>  "List",
                'data'        =>  $this->m_update_activity->getAll(),
                // 'action'      => "<a class='button button-blue' href='".base_url()."inspection/input_scrap/add'><i class='fa fa-plus'></i> Add</a>",
                'breadcrumb'  =>   array('<a>Inspection</a>','List'),
                'content'     =>  'inspection/update_activity/List'
            );
            $this->load->view('template/page', $data);
        } else {
            redirect(base_url().'login', 'refresh');
        }
    }


    public function edit($id = '') {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_rules('thd_lastactivity','Last Activity','trim|required');
            $activityext = $this->m_update_activity->getMasterGroup($id);
            if ($this->form_validation->run() == FALSE) {
                
                $data = array (
                    'pagetitle'   =>  "Input - Scrap",
                    'pos_parent'  =>  "inspection",
                    'pos_child'   =>  "inspection/Update_activity",
                    'title'       =>  "Inspection",
                    'subtitle'    =>  "Edit",
                    'action'      => "<a class='button button-red' href='".base_url()."inspection/update_activity'><i class='fa fa-times'></i> Cancel &amp; Discard</a>",
                    'breadcrumb'  =>   array('<a>Inspection</a>','Edit'),
                    // 'plugins_css'  => array('plugins/jquery-te/jquery-te-1.4.0.css'),
                    // 'plugins_js'   => array('plugins/jquery-te/jquery-te-1.4.0.min.js'),
                    'ma'          => $activityext,
                    'us'          => $this->m_update_activity->getActivity($activityext->THD_LOTEXT,$activityext->THD_LASTACTIVITY),
                    // 'brt'          => $this->m_berita->getBeritaId($id),
                    'content'     =>  'inspection/update_activity/Edit',

                );
                $this->load->view('template/page', $data);
            } else {
                
            //var_dump($this->input->post()); die();
                $activity=$this->m_update_activity->currentactivity($id);
                $data = array(
                    'THD_INSTRUCTIONINT'    => $this->input->post('thd_instructionint'),
                    'THD_LOTIN'             => $this->input->post('thd_lotin'),                    
                    'THD_LASTACTIVITY'      => $this->input->post('thd_lastactivity'),
                    'TDI_ENDACTIVITY'       => $this->input->post('tdi_endactivity'),
                    'TDI_ENDTIME'           => $this->input->post('tdi_endtime'),
                    'TDI_LASTUPDATE'        => $this->input->post('tdi_lastupdate'),
                    'TDI_PARENTID'          => $this->input->post('tdi_parentid'),
                    'TDP_ENDPAUSE'          => $this->input->post('tdp_endpause'),
                    'THD_LOTEXT'            => $activityext->THD_LOTEXT,
                    'THD_USERID'            => $this->input->post('thd_userid'),
                    'THD_BARCODE'           => $this->input->post('thd_barcode'),   
                    'THD_TABLEID'           => $this->input->post('thd_tableid'),
                    'PREVIEWACT'            => $activity->THD_LASTACTIVITY   
                );


                $this->m_update_activity->update($id,$data);
                $this->session->set_flashdata("pesan", "<div class='alert alert-success'>
                     <p><b>Success!</b> Inspection Update Activity Edited.<i class='fa fa-times'></i></p>
                 </div>");
                redirect(base_url() . 'inspection/update_activity', 'location');    
            }
        } else {
            //If no session, redirect to login page
            redirect(base_url().'login', 'refresh');
        }
    }

    // public function delete($id) {
    //     if ($this->session->userdata('loggedin')) {

    //         $this->m_update_activity->delete($id);
    //         $this->session->set_flashdata("pesan", "<div class='alert alert-success'>
    //             <p><b>Success!</b> Inspection Deleted.<i class='fa fa-times'></i></p>
    //         </div>");
    //         redirect(base_url().'inspection/input_scrap', 'refresh');
    //     } else {
    //         //If no session, redirect to login page
    //         redirect(base_url().'login', 'refresh');
    //     }
    // }
}
