<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        if ($this->session->userdata('loggedin')) { 
            $data = array(
            	'pagetitle'        => "Master Data - Dashboard",
            	'pos_parent'       => "dashboard",
            	'title'            => "Dashboard",
            	'subtitle'         => "dashboard",
            	'breadcrumb'       => array(),
                'content'          => 'dashboard/list',
            );
            $this->load->view('template/page', $data);
        } else {
            redirect(base_url().'login', 'refresh');
        }
    }

}
