<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        //$this->load->model('model_', '', TRUE);
    }

    public function index() {
        if ($this->session->userdata('loggedin')) {
            redirect('dashboard', 'refresh');
        } else {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('login');
            } else {
                redirect('dashboard', 'refresh');
            }
        }
    }

    public function check_database($password) {
        $this->load->model('model_', '', TRUE);
        $username = $this->input->post('username');

        $result = $this->model_->login($username, $password);

        if ($result) {
            $sess_array = array();
            foreach ($result as $row) {
                $sess_array = array(
                    'id'            => $row->UA_USERID,
                    'display_name'  => $row->UA_USERNAME,
                    'role'          => $row->UA_ROLESCOMMON,
                    'emplcode'      => $row->UA_EMPLCODE
                );
                // var_dump($sess_array);die();
                $this->session->set_userdata('loggedin', $sess_array);
            }
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Username and Password is not Valid!');
            return false;
        }
    }

    function logout() {
        $this->session->keep_flashdata('pesan_logout');
        $this->session->unset_userdata('loggedin');
        // session_destroy();
        redirect('login', 'refresh');
    }

}

?>
