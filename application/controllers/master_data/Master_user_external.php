<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_user_external extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_master_user_external', '',TRUE);
        $this->load->helper(array('form'));
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        if ($this->session->userdata('loggedin')) {
            $data = array (
                'pagetitle'   =>  "Master - User External",
                'pos_parent'  =>  "master_data",
                'pos_child'   =>  "master_user_external",
                'title'       =>  "Master Data",
                'subtitle'    =>  "List",
                'data'        =>  $this->m_master_user_external->getAll(),
                'action'      => "<a class='button button-blue' href='".base_url()."master_data/master_user_external/add'><i class='fa fa-plus'></i> Add</a>",
                'breadcrumb'  =>   array('<a>Master Table</a>','List'),
                'content'     =>  'master_data/master_user_external/List'
            );
            $this->load->view('template/page', $data);
        } else {
            redirect(base_url().'login', 'refresh');
        }
    }

    public function add() {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_rules('mae_username', 'Username', 'trim|required');
            $this->form_validation->set_rules('mae_company', 'Company Name', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data = array (
                    'pagetitle'   =>  "Master - Table",
                    'pos_parent'  =>  "master_data",
                    'pos_child'   =>  "master_data/master_user_external",
                    'title'       =>  "Master Data",
                    'subtitle'    =>  "Add",
                    'action'      => "<a class='button button-red' href='".base_url()."master_data/master_user_external'><i class='fa fa-times'></i> Cancel &amp; Discard</a>",
                    'breadcrumb'  => array('<a>Master Table</a>','Add'),
                    'plugins_css' => array('assets/plugins/select2/css/select2.min.css'),
                    'plugins_js'  => array('assets/plugins/select2/js/select2.min.js'),
                    'ma'          => $this->m_master_user_external->companylist(),
                    'content'     =>  'master_data/master_user_external/Add'
                );
                $this->load->view('template/page', $data);
            } else {
                $data = array(
                    'MAE_USERNAME'      => $this->input->post('mae_username'),
                    'MAE_COMPANY'       => $this->input->post('mae_company'),
                    'MAE_LASTUSER'      =>$this->session->userdata('loggedin')['emplcode'],
                    'MAE_LASTUPDATE'    =>date('Y-m-d H:i:s')
                );
                $this->m_master_user_external->insert($data);
                $this->session->set_flashdata("pesan", "<div class='alert alert-success'> 
                    <p><b>Success!</b> Input Master Table Added.<i class='fa fa-times'></i></p>
                </div>");
                        redirect(base_url() . 'master_data/master_user_external', 'location');
            }
        } else {
            redirect(base_url().'login', 'refresh');
        }
    }

    public function edit($id = '') {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_rules('mae_username', 'Username', 'trim|required');
            $this->form_validation->set_rules('mae_company', 'Company Name', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data = array (
                    'pagetitle'   =>  "Master - Table",
                    'pos_parent'  =>  "master_data",
                    'pos_child'   =>  "master_data/master_user_external",
                    'title'       =>  "Master Data",
                    'subtitle'    =>  "Edit",
                    'action'      => "<a class='button button-red' href='".base_url()."inspection/input_scrap'><i class='fa fa-times'></i> Cancel &amp; Discard</a>",
                    'breadcrumb'  =>   array('<a>Inspection</a>','Edit'),
                    'plugins_css' => array('assets/plugins/select2/css/select2.min.css'),
                    'plugins_js'  => array('assets/plugins/select2/js/select2.min.js'),
                    'ma'          => $this->m_master_user_external->getMasterGroup($id),
                    'us'          => $this->m_master_user_external->companylist(),
                    // 'brt'          => $this->m_berita->getBeritaId($id),
                    'content'     =>  'master_data/master_user_external/Edit',

                );
                $this->load->view('template/page', $data);
            } else {
                $data = array(
                    'MAE_USERNAME'    => $this->input->post('mae_username'),
                    'MAE_COMPANY'     => $this->input->post('mae_company')
                );

                $this->m_master_user_external->update($id,$data);
                $this->session->set_flashdata("pesan", "<div class='alert alert-success'>
                     <p><b>Success!</b> Master Table Edited.<i class='fa fa-times'></i></p>
                 </div>");
                redirect(base_url() . 'master_data/master_user_external', 'location');    
            }
        } else {
            //If no session, redirect to login page
            redirect(base_url().'login', 'refresh');
        }
    }

}
