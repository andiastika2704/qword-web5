<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_table extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_master_table', '',TRUE);
        $this->load->helper(array('form'));
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        if ($this->session->userdata('loggedin')) {
            $data = array (
                'pagetitle'   =>  "Master - Table",
                'pos_parent'  =>  "master_data",
                'pos_child'   =>  "master_table",
                'title'       =>  "Master Data",
                'subtitle'    =>  "List",
                'data'        =>  $this->m_master_table->getAll(),
                'action'      => "<a class='button button-blue' href='".base_url()."master_data/master_table/add'><i class='fa fa-plus'></i> Add</a>",
                'breadcrumb'  =>   array('<a>Master Table</a>','List'),
                'content'     =>  'master_data/master_table/List'
            );
            $this->load->view('template/page', $data);
        } else {
            redirect(base_url().'login', 'refresh');
        }
    }

    public function add() {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_rules('mt_location', 'Location', 'trim|required');
            $this->form_validation->set_rules('mt_secinitial', 'Section Initial', 'trim|required');
            $this->form_validation->set_rules('mt_column', 'Column', 'trim');

            if ($this->form_validation->run() == FALSE) {
                $data = array (
                    'pagetitle'   =>  "Master - Table",
                    'pos_parent'  =>  "master_data",
                    'pos_child'   =>  "master_data/Master_table",
                    'title'       =>  "Master Data",
                    'subtitle'    =>  "Add",
                    'action'      => "<a class='button button-red' href='".base_url()."master_data/master_table'><i class='fa fa-times'></i> Cancel &amp; Discard</a>",
                    'breadcrumb'  =>   array('<a>Master Table</a>','Add'),
                    'content'     =>  'master_data/master_table/Add'
                );
                $this->load->view('template/page', $data);
            } else {
                $mt_location    =$this->input->post('mt_location');
                $mt_secinitial  =$this->input->post('mt_secinitial');
                $mt_column      =$this->input->post('mt_column');
                $max=$this->m_master_table->checkmaxtableno($mt_secinitial,$mt_location,$mt_column);
                $subcolumn=substr($max->TABLENO,1);
                $mt_tableno=$mt_column.($subcolumn+1);

                //var_dump($subcolumn); die();

                $data = array(
                    'MT_LOCATION'   => $mt_location,
                    'MT_SECINITIAL' => $mt_secinitial,
                    'MT_TABLENO'    => $mt_tableno,
                    'MT_BARCODE'    => $mt_secinitial."-".$mt_location."-".$mt_tableno, 
                    'MT_STATUS'     => 1,
                    'MT_USERNAME'   => $this->session->userdata('loggedin')['emplcode'],
                    'MT_LASTUPDATE' => date('Y-m-d H:i:s')
                );
                if($this->sectioncode($mt_secinitial)[0]!="wrong code"){
                $data["MT_SECTIONSTART"]=$this->sectioncode($mt_secinitial)[0];
                $data["MT_SECTIONEND"]=$this->sectioncode($mt_secinitial)[1];                
                }
                //var_dump($data); die();
                $this->m_master_table->insert($data);
                $this->session->set_flashdata("pesan", "<div class='alert alert-success'> 
                    <p><b>Success!</b> Input Master Table Added.<i class='fa fa-times'></i></p>
                </div>");
                        redirect(base_url() . 'master_data/master_table', 'location');
            }
        } else {
            redirect(base_url().'login', 'refresh');
        }
    }

    public function edit($id = '') {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_rules('appname', 'Application Name', 'trim|required');
            $this->form_validation->set_rules('appdescription', 'Application Description', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data = array (
                    'pagetitle'   =>  "Master - Table",
                    'pos_parent'  =>  "master_data",
                    'pos_child'   =>  "master_data/master_table",
                    'title'       =>  "Master Data",
                    'subtitle'    =>  "Edit",
                    'action'      => "<a class='button button-red' href='".base_url()."inspection/input_scrap'><i class='fa fa-times'></i> Cancel &amp; Discard</a>",
                    'breadcrumb'  =>   array('<a>Inspection</a>','Edit'),
                    // 'plugins_css'  => array('plugins/jquery-te/jquery-te-1.4.0.css'),
                    // 'plugins_js'   => array('plugins/jquery-te/jquery-te-1.4.0.min.js'),
                    'ma'          => $this->m_master_table->getMasterGroup($id),
                    // 'brt'          => $this->m_berita->getBeritaId($id),
                    'content'     =>  'master_data/master_table/Edit',

                );
                $this->load->view('template/page', $data);
            } else {
                $data = array(
                    'APPNAME'           => $this->input->post('appname'),
                    'APPDESCRIPTION'    => $this->input->post('appdescription')
                );

                $this->m_master_table->update($id,$data);
                $this->session->set_flashdata("pesan", "<div class='alert alert-success'>
                     <p><b>Success!</b> Master Table Edited.<i class='fa fa-times'></i></p>
                 </div>");
                redirect(base_url() . 'master_data/master_table', 'location');    
            }
        } else {
            //If no session, redirect to login page
            redirect(base_url().'login', 'refresh');
        }
    }

    public function delete($id) {
        if ($this->session->userdata('loggedin')) {

            $this->m_master_table->delete($id);
            $this->session->set_flashdata("pesan", "<div class='alert alert-success'>
                <p><b>Success!</b> Master Table Deleted.<i class='fa fa-times'></i></p>
            </div>");
            redirect(base_url().'master_data/master_table', 'refresh');
        } else {
            //If no session, redirect to login page
            redirect(base_url().'login', 'refresh');
        }
    }

    function sectioncode($mt_secinitial){
        $return=array();
        if($mt_secinitial=='A1'){
            $return[0]=1110;
            $return[1]=1113;
        }
        elseif($mt_secinitial=='A2'){
            $return[0]=1120;
            $return[1]=1123;            
        }
        elseif($mt_secinitial=='A3'){
            $return[0]=1130;
            $return[1]=1133;            
        }
        elseif($mt_secinitial=='A4'){
            $return[0]=1140;
            $return[1]=1143;            
        }
        elseif($mt_secinitial=='S1'){
            $return[0]=1210;
            $return[1]=1213;            
        }
        elseif($mt_secinitial=='S2'){
            $return[0]=1220;
            $return[1]=1223;            
        }
        elseif($mt_secinitial=='M1'){
            $return[0]=1310;
            $return[1]=1313;            
        }
        elseif($mt_secinitial=='M2'){
            $return[0]=1320;
            $return[1]=1323;            
        }
        elseif($mt_secinitial=='PT'){
            $return[0]=1410;
            $return[1]=1423;            
        }
        elseif($mt_secinitial=='BR'){
            $return[0]=1510;
            $return[1]=1513;            
        }
        elseif($mt_secinitial=='PE'){
            $return[0]=3110;
            $return[1]=3131;            
        }
        elseif($mt_secinitial=='Q1'){
            $return[0]=2100;
            $return[1]=2113;            
        }
        elseif($mt_secinitial=='Q2'){
            $return[0]=2120;
            $return[1]=2122;            
        }else{
            $return[0]="wrong code";
        }
        return $return;

    }
}
