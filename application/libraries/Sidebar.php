<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Sidebar {

	public $pos_parent = '';
	public $pos_child = '';

	function setPosition($parent = '', $child = '') {
		$this->pos_parent = $parent;
		$this->pos_child = $child;
	}

	function createSingle($text, $link = '', $icon, $identifier) {
		if ($this->pos_parent == $identifier) {
			$status = "class='active'";
		} else {
			$status = "";
		}

		echo "<li ".$status."><a href='".base_url().$link."'>".$icon.$text."</a></li>";
	}

	function createNested($text, $icon, $identifier, $childs) {
		if ($this->pos_parent == $identifier) {
			$status = "active";
		} else {
			$status = "";
		}

		$anak = '';

		foreach ($childs as $c) {
			if($this->pos_parent == $identifier && $c[2] == $this->pos_child) {
				$status_child = "class='active'";
			} else {
				$status_child = "";
			}
			$anak = $anak."<li ".$status_child."><a href='".base_url().$c[1]."'>".$c[0]."</a></li>";
		}

		echo "<li class='second ".$status."'><a>".$icon.$text."<i class='fa arrow'></i></a>
                    <ul class='nav-second'>
                        ".$anak."
                    </ul>
                </li>";
	}

	function printMenu(){
		$CI =& get_instance();

		$CI->load->model('model_', '', TRUE);

		$menu = $CI->model_->userMenu($CI->session->userdata('loggedin')['id']);

		$keys = array_keys($menu);
		$child = [];
		foreach (array_keys($keys) as $k) {
			$menu_name = $menu[$keys[$k]]->UMG_GROUPNAME;
			$identifier = str_replace(' ', '_', strtolower($menu[$keys[$k]]->UMG_GROUPNAME));
			$child_menu = $menu[$keys[$k]]->MENUNAME;
			$url = str_replace(' ', '_', strtolower(trim($menu[$keys[$k]]->UMG_GROUPNAME))).'/'.str_replace(' ', '_', strtolower(trim($menu[$keys[$k]]->MENUNAME)));
			$child_identifier = str_replace(' ', '_', strtolower($menu[$keys[$k]]->MENUNAME));
			$m = '';
			// var_dump(count($menu) != 1+1 && 1 < 1);die();
			if (count($menu) != $k+1 && $k < 1) {
					// var_dump($menu[$keys[$k]]->UMG_GROUPNAME != $menu[$keys[$k+1]]->UMG_GROUPNAME);die();
				if($menu[$keys[$k]]->UMG_GROUPNAME != $menu[$keys[$k+1]]->UMG_GROUPNAME){
					if (count($child) == 0) {
						$this->createNested(
							$menu_name, 
							'<i class="fa fa-file-text-o"></i>', 
							$identifier, 
							array(
		                		array($child_menu, $url, $child_identifier)
		            		)
						);		
					} else {
						$this->createNested(
							$menu_name, 
							'<i class="fa fa-file-text-o"></i>', 
							$identifier, 
							$child
						);
						$child = [];
					}
				} else {
					$m = array($child_menu, $url, $child_identifier);
					array_push($child, $m);
				}
			} else {
				if(count($menu) == $k+1){
					if ($menu[$keys[$k]]->UMG_GROUPNAME != $menu[$keys[$k-1]]->UMG_GROUPNAME) {
							$this->createNested(
								$menu_name, 
								'<i class="fa fa-file-text-o"></i>', 
								$identifier, 
								array(
				                	array($child_menu, $url, $child_identifier)
				            	)
							);
					} else {
						$m = array($child_menu, $url, $child_identifier);
						array_push($child, $m);
						$this->createNested(
							$menu_name, 
							'<i class="fa fa-file-text-o"></i>', 
							$identifier, 
							$child
						);
						$child = [];
					}
				} else {
					// var_dump($menu[$keys[1]]->UMG_GROUPNAME != $menu[$keys[1+1]]->UMG_GROUPNAME);die();
					if($menu[$keys[$k]]->UMG_GROUPNAME != $menu[$keys[$k+1]]->UMG_GROUPNAME){
						// var_dump(count($child) == 0);die();
						if (count($child) == 0) {
							$this->createNested(
								$menu_name, 
								'<i class="fa fa-file-text-o"></i>', 
								$identifier, 
								array(
			                		array($child_menu, $url, $child_identifier)
			            		)
							);		
						} else {
							$m = array($child_menu, $url, $child_identifier);
							array_push($child, $m);
							$this->createNested(
								$menu_name, 
								'<i class="fa fa-file-text-o"></i>', 
								$identifier, 
								$child
							);
							$child = [];
						}
					} else {
						$m = array($child_menu, $url, $child_identifier);
						array_push($child, $m);
					}
				}
			}
		}
	}
}