<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('template/head'); ?>
</head>
<body>
    <div id='wrapper'>
        <?php $this->load->view('template/topnav'); ?>
        <?php $this->load->view('template/sidebar'); ?>
        <main>
            <?php $this->load->view('template/header'); ?>
            <?php
            if (isset($content)) {
                    $this->load->view($content);
            }
            ?>
        </main>
    </div>
</body>
</html>