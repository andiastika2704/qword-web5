<?php $this->sidebar->setPosition($pos_parent,isset($pos_child) ? $pos_child : ''); ?>
<aside>
    <div class='profil'>
        <div class='foto' style='background-image:url(<?=base_url()?>assets/uploads/profile_picture/<?php if (empty($this->session->userdata('loggedin')['foto'])){echo 'pp.jpg';}else {echo "$this->session->userdata('loggedin')['foto']";} ?>);'>
        </div>
        <div class='info'>
            <h4><?= ucwords($this->session->userdata('loggedin')['display_name']); ?></h4>
            <h5><?= $this->session->userdata('loggedin')['emplcode'] ?></h5>
        </div>
        <div class='opsi'>
            <div class='button-group'>
                <i class="fa fa-cog dropdown-toggle"></i>
                <ul class='dropdown-menu right-pos'>
                    <li class='head'>Profile Options</li>
                    <li><a href='<?= base_url() ?>profile'>Edit Profile</a></li>
                    <li class='sparator'></li>
                    <li><a href='<?= base_url() ?>login/logout'>Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
    <ul class='nav outer'>
        <?php
        /*

        Cara Membuat Menu Single
        $this->sidebar->createSingle(
            'Teksnya',
            'Urlnya',
            'Iconnya',
            'Pos_parent'
        );

        Cara Membuat Menu Nested
        $this->sidebar->createNested(
            'Teks Parentnya',
            'Iconnya',
            'Pos_parent',
            array(
                array('Teks child','Urlnya','Pos_child'),
                array('Teks child','Urlnya','Pos_child')
                ...dst
            )
        );

        *note : URLnya cukup url dibelakang imadmin. Jadi kalo misal URLnya http://localhost/avoskin/imadmin/product/add, cukup tuliskan 'product/add' saja.

        */
        $this->sidebar->createSingle(
            'Dashboard',
            '',
            '<i class="fa fa-tachometer"></i>',
            'dashboard'
        );

        $this->sidebar->printMenu();

        // $this->sidebar->createNested(
        //     'Fokus Berita',
        //     '<i class="fa fa-file-text-o"></i>',
        //     'fokusBerita',
        //     array(
        //         array('List Fokus Berita','fokusBerita','list'),
        //         array('Tambah Fokus Berita','fokusBerita/add','add')

        //     )
        // );

        // if ($this->session->userdata('loggedin')['role'] == 'admin') {
        //     $this->sidebar->createNested(
        //         'Pengguna',
        //         '<i class="fa fa-user"></i>',
        //         'pengguna',
        //         array(
        //             array('List Pengguna','pengguna','list'),
        //             array('Tambah Pengguna','pengguna/add','add')
        //         )
        //     );
        // }

        // if ($this->session->userdata('loggedin')['role'] == 'admin' || $this->session->userdata('loggedin')['role'] == 'redaktur') {
        //     $this->sidebar->createSingle(
        //         'Kategori',
        //         'category',
        //         '<i class="fa fa-list-alt"></i>',
        //         'category'
        //     );
        // }
        // $this->sidebar->createNested(
        //     'Berita',
        //     '<i class="fa fa-file-text-o"></i>',
        //     'berita',
        //     array(
        //         array('List Berita','berita','list'),
        //         array('Tambah Berita','berita/add','add')

        //     )
        // );

        // $this->sidebar->createNested(
        //     'Fokus Berita',
        //     '<i class="fa fa-file-text-o"></i>',
        //     'fokusBerita',
        //     array(
        //         array('List Fokus Berita','fokusBerita','list'),
        //         array('Tambah Fokus Berita','fokusBerita/add','add')

        //     )
        // );
        ?>
    </ul>
</aside>
