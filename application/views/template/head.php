<title><?= $pagetitle; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/fa/css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/jquery-ui.css"/>
    <link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/plugins/datatables/css/jquery.datatables.min.css'/>
    <link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/imadmin.css'/>
    <?php
        if (isset($plugins_css)) {
            foreach ($plugins_css as $css) {
                echo '<link rel="stylesheet" type="text/css" href="'.base_url().$css.'"/>
                ';
            }
        }
    ?>
    <script src="<?= base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery-ui.min.js"></script>
    <script src="<?= base_url() ?>assets/plugins/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea.wysiwyg",
            min_height: 200,
            plugins: "image link table textcolor",
            toolbar: ["bold italic underline subscript superscript | fontsizeselect forecolor backcolor", "cut copy paste | bullist numlist outdent indent removeformat | link image table"]
        });
    </script>
    <script src="<?= base_url() ?>assets/plugins/datatables/js/jquery.datatables.min.js"></script>
    <script src="<?= base_url() ?>assets/js/script.js"></script>

    <?php
        if (isset($plugins_js)) {
            foreach ($plugins_js as $js) {
                echo '<script src="'.base_url().$js.'"></script>
                ';
            }
        }
    ?>
	