<nav>
    <div class='logo-wrap'>
        <img src='<?= base_url() ?>assets/images/user.png'/>
        <div class='text'>
            <h3 class='top'>
                <span>Q-WORD</span>
            </h3>
            <h4 class='bottom'>by SYSTEM DEPT.</h4>
        </div>
        <i class="fa fa-bars side-toggle tipb" title='Toggle Sidebar'></i>
    </div>
    <!-- <a href='<?= base_url() ?>' target='_blank'><h4 class='button button-red view-website'><i class='fa fa-desktop'></i> View Website</h4></a> -->
</nav>