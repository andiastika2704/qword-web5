  <?= $this->session->flashdata('pesan') ?>
<script>
$(document).ready(function(){
  $('#thd_abnormal1').change(function(){
    var ABL1_Code=$(this).val();
    $.ajax({
    url:'<?=base_url("inspection/input_scrap/getabnormal2")?>',
    method:"POST",
    data:{ABL1_Code:ABL1_Code},
    dataType:"text",
    success:function(data)
    {
      $('#thd_abnormal2').html(data);
    }
    });
  });

});

$(document).ready(function(){
    $('#thd_abnormal2').change(function(){
        var ABL2_Code=$(this).val();
        $.ajax({
        url:'<?=base_url("inspection/input_scrap/getabnormal3")?>',
        method:"POST",
        data:{ABL2_Code:ABL2_Code},
        dataType:"text",
        success:function(data)
        {
            $('#thd_abnormal3').html(data);
        }
        });
    });

});

</script>
  <div class='row'>
      <?= form_open_multipart(base_url()."inspection/input_scrap/edit/$ma->THD_ID")?>
      <div class='col-2'>
          <div class='panel'>
              <div class='panel-body'>
                <div class='input-row'> 
                    <h5>ID:</h5>
                    <input type="text" name="thd_id" <?= form_error('thd_id') ?> value="<?= $ma->THD_ID ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>Employee Name :</h5>
                    <input type="text" name="thd_userid" <?= form_error('thd_userid') ?> value="<?= $ma->EM_EMPLNAME ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>Product Name :</h5>
                    <input type="text" name="partbom_partname" value="<?= $ma->PARTBOM_PARTNAME ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>                                  
                <div class='input-row'>
                    <h5>PI Lot External :</h5>
                    <input type="text" name="thd_instructionext" <?= form_error('thd_instructionext') ?> value="<?= $ma->THD_INSTRUCTIONEXT ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>PI Internal:</h5>
                    <input type="text" name="thd_instructionint" <?= form_error('thd_instructionint') ?> value="<?= $ma->THD_INSTRUCTIONINT ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>Lot Internal :</h5>
                    <input type="text" name="thd_lotin" <?= form_error('thd_lotin') ?> value="<?= $ma->THD_LOTIN ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>
               
              </div>
          </div>
      </div>
      <div class='col-2'>
          <div class='panel'>
              <div class='panel-body'>
                <div class='input-row'>
                    <h5>Barcode :</h5>
                    <input type="text" name="thd_barcode" <?= form_error('thd_barcode') ?> value="<?= $ma->THD_BARCODE ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>                  
                <div class='input-row'>
                    <h5>Judgement :</h5>
                    <select class="form-control" name="thd_judgement">
                        <option value="0" disabled="disabled">-SELECT JUDGEMENT-</option>
                        <option value="1" <?= $ma->THD_JUDGEMENT==1 ? "selected" : "" ?>>ACCEPTANCE</option>
                        <option value="2" <?= $ma->THD_JUDGEMENT==2 ? "selected" : "" ?>>REJECTION</option>
                    </select>

                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>Inspection Qty:</h5>
                    <input type="text" name="thd_inspectionqty" <?= form_error('thd_inspectionqty') ?> value="<?= $ma->THD_INSPECTIONQTY ?>">
                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>Qty NG :</h5>
                    <input type="text" name="thd_qtyng" <?= form_error('thd_qtyng') ?> value="<?= $ma->THD_QTYNG ?>">
                    <div class="hidden" style='display:none;'></div>
                </div>                                            
                 <div class='input-row submit'>
                    <input type='submit' value='Submit' class='button button-blue'/>
                 </div>
              </div>
          </div>
      </div>

          <?= form_close()?>
  </div>
