<?= $this->session->flashdata('pesan') ?>
<!--           <script type = "text/JavaScript">
 
            $(document).ready(function() {
                var table=$('#input_scrap').DataTable( {  
                    destroy: true,      
                    'aoColumnDefs'  : [{
                    'bSortable'     : false,
                    'aTargets'      : ['nosort'] } ],    
                    dom             : 'Bfrtip',
                    buttons         : ['excel']
                } );

            $('#input_scrap thead tr:last th').each( function () {
                var title = $('#input_scrap tfoot th').eq( $(this).index() ).text();
                $(this).html( '<input style="color:black;" size="3" type="text" placeholder="Search '+title+'" />' );
            } );

            // Apply the search
            table.columns().eq( 0 ).each( function ( colIdx ) {
                    $( 'input', table.column( colIdx ).header() ).on( 'keyup change', function () {
                        table
                            .column( colIdx )
                            .search( this.value )
                            .draw();
                    } );
                } );
                        } );

          </script> -->

<div class='row'>
    <div class='col-4'>
        <div class='panel'>
            <div class='panel-head'>
                <h5>Input Scrap</h5>
            </div>
            <a href="<?= base_url()."inspection/input_scrap/export_excel" ?>"><button type="button" class="btn btn-success">Export</button></a>
            <div class='panel-body'>
                <table class='bordered table-green datatable' id="input_scrap">
                    <thead>
                        <tr>
				            <th>ID</th>
                            <th>PI Lot Ext.</th>
                            <th>PI Int.</th>
                            <th>Lot Int.</th>
                            <th>Barcode</th>
                            <th>Judgement</th>
                            <th>Inspection Qty</th>
                            <th>QtyNG</th>                            
                            <th>Empl.</th>
                            <th>Last Update</th>                                   
                            <th class='nosort'>Action</th>
                        </tr>
                        <tr>
                            <th>ID</th>
                            <th>PI Lot Ext.</th>
                            <th>PI Int.</th>
                            <th>Lot Int.</th>
                            <th>Barcode</th>
                            <th>Judgement</th>
                            <th>Inspection Qty</th>
                            <th>QtyNG</th>                            
                            <th>Empl.</th>
                            <th>Last Update</th>                        
                            <th></th>
                        </tr>                        
                    </thead>
                    <tbody>
                        <?php
                        // var_dump($brt);die;


                        foreach ($data as $d) {
                        	echo "<tr align='center'>
                            <td>".$d->THD_ID."</td>
                            <td>".$d->THD_INSTRUCTIONEXT."</td>
                            <td>".$d->THD_INSTRUCTIONINT."</td>
                            <td>".$d->THD_LOTIN."</td>
                            <td>".$d->THD_BARCODE."</td>
                            <td>".$d->JUDGEMENT."</td>
                            <td>".$d->THD_INSPECTIONQTY."</td>
                            <td>".$d->THD_QTYNG."</td>
                            <td>".$d->EM_EMPLNAME."</td> 
                            <td>".$d->THD_LASTUPDATE."</td>                                                       
                            <td class='nowrap'>
                              ".anchor(base_url()."inspection/input_scrap/edit/$d->THD_ID", "<i class='fa fa-pencil'></i>", "class='button button-yellow'")."
                            </td>
                        </tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
