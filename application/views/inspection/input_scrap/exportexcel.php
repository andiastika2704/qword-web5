<?php 
$lastdownload     =date('Y-m-d');
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=Q-WORD.xls");
?>
        <table class='bordered table-green datatable' id="input_scrap">
                    <thead>
                        <tr>
				            <th>ID</th>
                            <th>PI Ext.</th>
                            <th>Lot Ext.</th>
                            <th>PI Int.</th>
                            <th>Lot Int.</th>
                            <th>Barcode</th>
                            <th>Judgement</th>
                            <th>Inspection Qty</th>
                            <th>QtyNG</th>
                            <th>Empl.Code</th>                            
                            <th>Empl.</th>
                            <th>Activity.</th>
                            <th>Category.</th>
                            <th>Start Time.</th>
                            <th>Activity.</th>
                            <th>Category</th>
                            <th>End Time</th>
                            <th>Start Pause Time</th>
                            <th>End Pause Time</th>                           
                        </tr>                       
                    </thead>
                    <tbody>
                        <?php
                        // var_dump($brt);die;


                        foreach ($data as $d) {
                            $NO=1;
                        	echo "<tr align='center'>
                            <td></td>
                            <td>".$d->THD_INSTRUCTIONEXT."</td>
                            <td>".$d->THD_LOTEXT."</td>
                            <td>".$d->THD_INSTRUCTIONINT."</td>
                            <td>".$d->THD_LOTIN."</td>
                            <td>".$d->THD_BARCODE."</td>
                            <td>".$d->JUDGEMENT."</td>
                            <td>".$d->THD_INSPECTIONQTY."</td>
                            <td>".$d->THD_QTYNG."</td>
                            <td>".$d->TDI_USERID."</td>
                            <td>".$d->EM_EMPLNAME."</td> 
                            <td>".$d->STARTNAME."</td> 
                            <td>".$d->STARTCATEGORY."</td> 
                            <td>".$d->TDI_STARTTIME."</td> 
                            <td>".$d->ENDNAME."</td> 
                            <td>".$d->ENDCATEGORY."</td> 
                            <td>".$d->TDI_ENDTIME."</td> 
                            <td>".$d->TDP_STARTPAUSE."</td> 
                            <td>".$d->TDP_ENDPAUSE."</td> 
                        </tr>";
                        }
                        ?>
                    </tbody>
        </table>