  <?= $this->session->flashdata('pesan') ?>
  <div class='row'>
      <?= form_open(base_url()."inspection/update_activity/edit/$ma->THD_ID")?>
      <div div class='col-2'>
          <div class='panel'>
              <div class='panel-body'>
                <div class='input-row'>
                    <h5>ID :</h5>
                    <input type="text" name="thd_id" <?= form_error('thd_id') ?> value="<?= $ma->THD_ID ?>" readonly >
                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>User ID :</h5>
                    <input type="text" name="thd_userid" <?= form_error('thd_userid') ?> value="<?= $ma->THD_USERID ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>Empl Name. :</h5>
                    <input type="text" name="em_emplname" <?= form_error('em_emplname') ?> value="<?= $ma->EM_EMPLNAME ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>PI Ext :</h5>
                    <input type="text" name="thd_instructionext" <?= form_error('thd_instructionext') ?> value="<?= $ma->THD_INSTRUCTIONEXT ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>Lot Ext :</h5>
                    <input type="text" name="thd_lotext" <?= form_error('thd_lotext') ?> value="<?= $ma->THD_LOTEXT ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>
                 <div class='input-row'>
                    <h5>PI Int :</h5>
                    <input type="text" name="thd_instructionint" <?= form_error('thd_instructionint') ?> value="<?= $ma->THD_INSTRUCTIONINT ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>Lot In :</h5>
                    <input type="text" name="thd_lotin" <?= form_error('thd_lotin') ?> value="<?= $ma->THD_LOTIN ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div>
              </div>
          </div>
      </div>
      <div div class='col-2'>
          <div class='panel'>
              <div class='panel-body'>
                <div class='input-row'>
                    <h5>Barcode :</h5>
                    <input type="text" name="thd_barcode" <?= form_error('thd_barcode') ?> value="<?= $ma->THD_BARCODE ?>" readonly>
                    <div class="hidden" style='display:none;'></div>
                </div> 
                <div class='input-row'>
                    <h5>Table :</h5>
                    <input type="text" name="mt_barcode" <?= form_error('mt_barcode') ?> value="<?= $ma->MT_BARCODE ?>" readonly>
                    <input type="text" name="thd_tableid" <?= form_error('thd_tableid') ?> value="<?= $ma->THD_TABLEID ?>" hidden>                    
                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>Activity :</h5>
                    <select class="form-control" name="thd_lastactivity">
                        <option value="0" disabled="disabled">-SELECT ACTIVITY-</option>
                        <?php 
                          foreach ($us as $d) {
                        ?>
                        <option value="<?= $d->MA_ID ?>" <?= $d->MA_ID == $ma->THD_LASTACTIVITY ? "selected disabled" : "" ?>> <?= $d->MA_NAME ." - ". $d->MA_CATEGORY ?> </option>
                        <?php } ?>
                    </select>
                </div>                                
                 <div class='input-row submit'>
                    <input type='submit' value='Edit' class='button button-blue'/>
                 </div>
              </div>
          </div>
      </div>
          <?= form_close()?>
  </div>
