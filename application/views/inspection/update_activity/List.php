<?= $this->session->flashdata('pesan') ?>

<div class='row'>
    <div class='col-4'>
        <div class='panel'>
            <div class='panel-head'>
                <h5>Update Activity</h5>
            </div>
            <div class='panel-body'>
                <table class='bordered table-green datatable' id="update_activity">
                    <thead>
                        <tr>
                            <th>ID</th>
				            <th>Empl.Code</th>
                            <th>Empl.Name</th>                                   
                            <th>PI Ext.</th>
                            <th>Lot Ext.</th>
                            <th>PI Int.</th>
                            <th>Lot Int.</th>
                            <th>Barcode</th>    
                            <th>Table</th>                                                    
                            <th>In Progress</th>
                            <th class='nosort'>Action</th>
                        </tr>
                        <tr>
                            <th>ID</th>
                            <th>Empl.Code</th>
                            <th>Empl.Name</th>                                   
                            <th>PI Ext.</th>
                            <th>Lot Ext.</th>
                            <th>PI Int.</th>
                            <th>Lot Int.</th>
                            <th>Barcode</th>    
                            <th>Table</th>                                                    
                            <th>In Progress</th>
                            <th></th>
                        </tr>                        
                    </thead>
                    <tbody>
                        <?php
                        // var_dump($brt);die;

                        foreach ($data as $d) {
                        	echo "<tr>
                            <td>".$d->THD_ID."</td>
                            <td>".$d->THD_USERID."</td>
                            <td>".$d->EM_EMPLNAME."</td>
                            <td>".$d->THD_INSTRUCTIONEXT."</td>
                            <td>".$d->THD_LOTEXT."</td>
                            <td>".$d->THD_INSTRUCTIONINT."</td>
                            <td>".$d->THD_LOTIN."</td>
                            <td>".$d->THD_BARCODE."</td>
                            <td>".$d->MT_BARCODE."</td>                            
                            <td>".$d->MA_NAME."</td>
                            <td class='nowrap'>
                              ".anchor(base_url()."inspection/update_activity/edit/$d->THD_ID", "<i class='fa fa-pencil'></i>", "class='button button-yellow'")."
                            </td>
                        </tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
