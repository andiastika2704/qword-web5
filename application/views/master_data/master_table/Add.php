  <?= $this->session->flashdata('pesan') ?>
  <div class='row'>
      <?= form_open_multipart(base_url()."master_data/master_table/add")?>
      <div class='col-1of3'>
          <div class='panel'>
              <div class='panel-body'>
                <div class='input-row'>
                <div class="form-group">
                    <h5>Location :</h5>
                    <select class="form-control" name="mt_location">
                        <option value="0">Select Location</option>
                        <option value="1">Phase 1</option>
                        <option value="2">Phase 2</option>
                        <option value="3">Phase 3</option>
                        <option value="4">Phase 4</option>
                        <option value="5">Phase 5</option>
                        <option value="6">Phase 6</option>
                    </select>
                </div>
                </div>
                <div class='input-row'>
                <div class="form-group">
                    <h5>Section :</h5>
                    <select class="form-control" name="mt_secinitial">
                        <option value="0">Select Section</option>
                        <option value="A1">Assembly 1</option>
                        <option value="A2">Assembly 2</option>
                        <option value="A3">Assembly 3</option>
                        <option value="A4">Assembly 4</option>
                        <option value="S1">Stamping 1</option>
                        <option value="S2">Stamping 2</option>
                        <option value="M1">Molding 1</option>
                        <option value="M2">Molding 2</option>
                        <option value="PT">Plating Line / Barrel</option>
                        <option value="BR">Brazing</option>
                        <option value="PE">Production Engineering</option>
                        <option value="Q1">Quality Control 1</option>
                        <option value="Q2">Quality Control 2</option>  
                    </select>
                </div>
                </div>
                <div class='input-row'>
                <div class="form-group">
                    <h5>Coloumn :</h5>
                    <select class="form-control" name="mt_column">
                        <option value="0">Select Coloumn</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                        <option value="E">E</option>
                        <option value="F">F</option>
                    </select>
                </div>                
                </div>          
                 <div class='input-row submit'>
                    <input type='submit' value='Save' class='button button-blue'/>
                 </div>
              </div>
          </div>
      </div>
          <?= form_close()?>
  </div>