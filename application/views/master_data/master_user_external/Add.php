  <?= $this->session->flashdata('pesan') ?>
  <script type="text/javascript">
    $(document).ready(function() {
        $('select').select2();
    });
 </script>
  <div class='row'>
      <?= form_open_multipart(base_url()."master_data/master_user_external/add")?>
      <div class='col-1of3'>
          <div class='panel'>
              <div class='panel-body'>
                <div class='input-row'>
                    <h5>Username :</h5>
                    <input type="text" name="mae_username" <?= form_error('mae_username') ?> value="<?= set_value('mae_username')?>">
                    <p class="helper">* Require</p>
                    <div class="hidden" style='display:none;'></div>
                </div>
                <div class='input-row'>
                    <h5>Company :</h5>
                    <select class="form-control" name="mae_company">
                        <option value="0">-SELECT COMPANY-</option>
                        <?php 
                          foreach ($ma as $d) {
                        ?>
                        <option value="<?= $d->SPL_SUPCODE ?>"> <?= $d->SPL_SUPNAME ?> </option>
                        <?php } ?>
                    </select>
                    <p class="helper">* Require</p>
                    <div class="hidden" style='display:none;'></div>
                </div>
                 <div class='input-row submit'>
                    <input type='submit' value='Save' class='button button-blue'/>
                 </div>
              </div>
          </div>
      </div>
          <?= form_close()?>
  </div>
