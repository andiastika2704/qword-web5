<?= $this->session->flashdata('pesan') ?>

<div class='row'>
    <div class='col-4'>
        <div class='panel'>
            <div class='panel-head'>
                <h5>User Access</h5>
            </div>
            <div class='panel-body'>
                <table class='bordered table-green datatable'>
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Username</th>
                            <th>Supplier Company</th>
                            <th class='nosort'>Action</th>
                        </tr>
                        <tr>
                            <th>User ID</th>
                            <th>Username</th>
                            <th>Supplier Company</th>
                            <th></th>
                        </tr>                        
                    </thead>
                    <tbody>
                        <?php
                        // var_dump($brt);die;

                        foreach ($data as $d) {
                        	echo "<tr>
                            <td>".$d->MAE_USERID."</td>
                            <td>".$d->MAE_USERNAME."</td>
                            <td>".$d->SPL_SUPNAME."</td>
                            <td class='nowrap'>
                              ".anchor(base_url()."master_data/master_user_external/edit/$d->MAE_USERID", "<i class='fa fa-pencil'></i>", "class='button button-yellow'")."
                            </td>
                        </tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
